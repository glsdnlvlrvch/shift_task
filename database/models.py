from sqlalchemy import String, Integer, Float, ForeignKey
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship


class Base(DeclarativeBase):
    pass


class User(Base):
    __tablename__ = "user"

    id: Mapped[int] = mapped_column(primary_key=True)
    username: Mapped[str] = mapped_column(String(10))
    password: Mapped[str] = mapped_column(String(60))

    salary_info: Mapped["SalaryInfo"] = relationship(back_populates="user")


class SalaryInfo(Base):
    __tablename__ = "salary_info"

    id: Mapped[int] = mapped_column(primary_key=True)
    current_salary: Mapped[int] = mapped_column(Integer)
    next_promote: Mapped[float] = mapped_column(Float)
    user_id: Mapped[int] = mapped_column(ForeignKey("user.id"))

    user: Mapped["User"] = relationship(back_populates="salary_info")
