import sqlalchemy as sa

from .models import User, SalaryInfo, Base


class DatabaseManager:
    def __init__(self, url: str):
        self.engine = sa.create_engine(url, echo=True)
        self.conn = self.engine.connect()

        Base.metadata.create_all(self.engine)

    def get_salary(self, user_id: int):
        query = sa.select(SalaryInfo).where(SalaryInfo.user_id == user_id)
        return self.conn.execute(query)

    def create_user(self, username: str, password: str):
        self.conn.execute(
            sa.insert(User).values(username=username, password=password)
        )
        self.conn.commit()

    def get_user(self, username: str):
        return self.conn.execute(
            sa.select(User).where(User.username == username)
        ).all()

    def insert_salary_data(self, user_id: int, curr_salary: int, next_promote: float):
        self.conn.execute(sa.insert(SalaryInfo).values(
            user_id=user_id,
            current_salary=curr_salary,
            next_promote=next_promote
        ))
        self.conn.commit()

    def reconnect(self, url: str):
        """For tests only"""
        self.__init__(url)
