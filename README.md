# SHIFT Test Task

This is a project that fetches the salary info for the workers. <br> Unfortunately, **poetry** does not support "_dotenv_" package so **pip** package manager is used.

## Installation 
1. Install the dependencies using poetry <br> `pip install -r requirements.txt` <br>
2. Setup PostgreSQL database and configure connection in `.env` according `.env.example` <br>
3. Start backend server using **uvicorn** <br> `uvicorn main:app --reload` <br>

## API Endpoints
**_/register_** - create worker account. Request should contain json with '_username_' and '_password_'. <br>
**_/login_** - get access token. Request should also contain json with '_username_' and '_password_'. <br>
**_/get_salary_** - get your salary. Request should contain parameter '_token_'.
