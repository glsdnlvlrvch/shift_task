from os import getenv
from dotenv import load_dotenv

load_dotenv()

# DB URL for connecting to PostgreSQL
DB_URL = getenv('DB_URL')

# JWT parameters
SECRET_KEY = getenv('SECRET_KEY')
ALGORITHM = getenv('ALGORITHM')
ACCESS_TOKEN_EXPIRE_MINUTES = int(getenv('ACCESS_TOKEN_EXPIRE_MINUTES'))
