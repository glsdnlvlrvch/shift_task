from fastapi import HTTPException, status
from jose import JWTError, jwt
from datetime import datetime, timedelta


class JwtService:
    def __init__(
            self,
            access_token_expire_minutes: float,
            secret_key: str,
            algorithm: str
    ):
        self.access_token_expire_minutes = access_token_expire_minutes
        self.secret_key = secret_key
        self.algorithm = algorithm

    def create_access_token(self, data: dict):
        """Create access token"""
        expire = datetime.utcnow() + timedelta(minutes=self.access_token_expire_minutes)

        to_encode = {
            **data,
            "exp": expire
        }

        return jwt.encode(to_encode, self.secret_key, algorithm=self.algorithm)

    def verify_token(self, token: str):
        """Verify access token"""
        try:
            payload = jwt.decode(token, self.secret_key, algorithms=[self.algorithm])
            return payload
        except JWTError:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Invalid access token",
            )
