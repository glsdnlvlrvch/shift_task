import datetime

from fastapi import FastAPI, HTTPException, status

from settings import ACCESS_TOKEN_EXPIRE_MINUTES, DB_URL, SECRET_KEY, ALGORITHM
from database.db_manager import DatabaseManager
from services.jwt import JwtService
from services.auth import AuthService
from schemas.user import CreateUserScheme, LogInUserScheme

# Initialise the app
app = FastAPI()

# Initialise the database
db = DatabaseManager(DB_URL)

# Initialise the services
auth = AuthService()
jwt = JwtService(ACCESS_TOKEN_EXPIRE_MINUTES, SECRET_KEY, ALGORITHM)


# the endpoint to get the token
@app.post("/login")
async def login(log_in_user_scheme: LogInUserScheme):
    # if user is not exist, raise exception
    result = db.get_user(log_in_user_scheme.username)
    if len(result) == 0:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="User with this username is not exists"
        )

    # if password is incorrect, raise exception
    if not auth.verify_password(log_in_user_scheme.password, result[0][2]):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Incorrect password")

    # creating access token
    token = jwt.create_access_token(data={
        'username': log_in_user_scheme.username,
        'password': log_in_user_scheme.password
    })

    return {'token': token}


# the endpoint to verify the token
@app.get("/verify_token")
async def verify_token(token: str):
    return jwt.verify_token(token)


@app.post("/register")
async def register(create_user_scheme: CreateUserScheme):
    # if user already exists, raise exception
    result = db.get_user(
        create_user_scheme.username
    )

    if len(result) > 0:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="User with this username already exists"
        )

    # creating user
    db.create_user(
        create_user_scheme.username,
        auth.get_password_hash(create_user_scheme.password)
    )

    return create_user_scheme


@app.get('/get_salary')
async def get_salary(token: str):
    # verify token
    payload = jwt.verify_token(token)

    # get salary data
    user_data = db.get_user(payload['username'])[0]
    salary_data = db.get_salary(user_data[0]).all()

    if salary_data:
        return {
            "current_salary": salary_data[0][1],
            "next_promotion": salary_data[0][2]
        }
    else:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="No salary data found")


@app.post('/mock_salary')
async def mock_salary(token: str):
    # verify token
    payload = jwt.verify_token(token)

    # get user data
    user_data = db.get_user(payload['username'])[0]

    salary_data = db.get_salary(user_data[0]).all()
    if salary_data:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail='already mocked')

    # insert mock salary data
    user_id = user_data[0]
    current_salary = 1000
    next_promote = datetime.datetime.now().timestamp() + 60*60*24*90
    db.insert_salary_data(user_id, current_salary, next_promote)

    return {
        'current_salary': current_salary,
        'next_promote': next_promote
    }
