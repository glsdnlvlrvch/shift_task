from pydantic import BaseModel


class CreateUserScheme(BaseModel):
    username: str
    password: str


class LogInUserScheme(BaseModel):
    username: str
    password: str
