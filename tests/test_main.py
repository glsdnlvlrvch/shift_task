import pytest
from sqlalchemy.orm import sessionmaker
from fastapi.testclient import TestClient
from os import getenv

from main import app, db

# configuring the database for testing
db.reconnect(getenv('DB_TEST_URL'))

# # configuring session for rolling back testing db
Session = sessionmaker()

transaction = db.conn.begin()
session = Session(bind=db.conn)
db.conn = session

# Initializing the testing FastAPI client
client = TestClient(app)


@pytest.fixture()
def user_data():
    return {
        'username': 'sato',
        'password': '01012001Sato'
    }


def test_create_user(user_data):
    with db.conn.begin() as trans:
        # successful registering
        response = client.post('/register', json=user_data)
        assert response.status_code == 200
        assert response.json() == user_data

    # unsuccessful registering
    response = client.post('/register', json=user_data)
    assert response.status_code == 500
